package ca.jhoffman.cours10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    public static String INTENT_DATA_KEY = "INTENT_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        String intentData = getIntent().getStringExtra(INTENT_DATA_KEY);
        TextView intentDataTextView = (TextView)findViewById(R.id.second_textview_intentData);
        intentDataTextView.setText(intentData);
    }
}
