package ca.jhoffman.cours10;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("DEMO", "onCreate");

        setContentView(R.layout.activity_main);

        Button goToSecond = (Button)findViewById(R.id.main_button_goToSecond);
        goToSecond.setOnClickListener(goToSecondClickListener);


        if (savedInstanceState != null) {
            Log.d("DEMO", savedInstanceState.getString("TEST"));
        }
    }

    private View.OnClickListener goToSecondClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(MainActivity.this, SecondActivity.class);
            Date date = new Date();
            i.putExtra(SecondActivity.INTENT_DATA_KEY, "test data: " + date);
            startActivity(i);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString("TEST", "testetst");
        super.onSaveInstanceState(outState);

        Log.d("DEMO", "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.d("DEMO", "onRestoreInstanceState");
    }

    @Override
    protected void onPause() {
        super.onPause();


        Log.d("DEMO", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("DEMO", "onStop");


    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("DEMO", "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d("DEMO", "onRestart");
    }
}
